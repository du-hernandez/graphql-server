/**
 * Servidor
 */

const express = require("express");
const app = express();

/**
 * Integración con GraphQL
 */
const express_graphql = require("express-graphql")
// Para construir un schema
const { buildSchema } = require("graphql")

// Data
const { courses } = require('./data.json');

const schema = buildSchema(`
    type Query {
        course(id: Int!): Course
        courses(topic: String): [Course]
    }

    type Mutation {
        updateCourseTopic(id: Int!, topic: String): Course
    }

    type Course {
        id: Int
        title: String
        author: String
        topic: String
        url: String
    }
`);

// course(id: Int!): Course
let getCourse = (args) => {
    let id = args.id;
    return courses.filter(course => {
        return course.id == id;
    })[0]
}

// courses(topic: String): [Course]
let getCourses = (args) => {
    if (args.topic) {
        return courses.filter(course => {
            return course.topic === args.topic
        })
    } else {
        return courses
    }
}

// type Mutation { ... }
let updateCourseTopic = ({ id, topic }) => {
    courses.map(course => {
        if (course.id === id) {
            course.topic = topic;
            return course;
        }
    })
    return courses.filter(course => course.id === id)[0]
}

// Define qué se puede consultar
const root = {
    course: getCourse,
    courses: getCourses,
    updateCourseTopic: updateCourseTopic
}

app.use("/graphql", express_graphql(
    {
        schema: schema,
        rootValue: root,
        // Interactual con GraphQL
        graphiql: true
    }
))

app.listen(3000, () => console.log("Server on por 3000"))